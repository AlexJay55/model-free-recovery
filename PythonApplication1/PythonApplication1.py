import numpy as np
from numpy.linalg import inv
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import style
style.use('ggplot')
from scipy.optimize import differential_evolution as de
import numbers

def calc_Delta_Hedged_R(R,p):
    def f_R(R,p,derivative = 0):
    #convex payoff function and derivatives
        if derivative == 0:
            return (R**p-1)/(p*(p-1))
        if derivative == 1:
            return (R**(p-1))/(p-1)
        if derivative == 2:
            return R**(p-2)
    DfR = f_R(R,p)-f_R(1,p)-f_R(1,p,derivative = 1)*(R-1)
    return DfR

def calc_EQ_DPR(options,moment_vector,p):
    
    def sum_puts(options,p):
        put_data = options[(options['putcall'] == 'P') & (options['forwardMoneyness'] < 1)]
        weights = f_R(put_data['forwardMoneyness'].as_matrix(),p,derivative = 2)
        strike_diff = np.append(put_data['forwardMoneyness'].as_matrix()[1:], [1.0]) - put_data['forwardMoneyness'].as_matrix()
        return np.sum(weights*put_data['implPrice']*strike_diff)
    
    def sum_calls(options,p):
        call_data = options[(options['putcall'] == 'C') &    (options['forwardMoneyness']>=1)]
        weight = f_R(call_data['forwardMoneyness'].as_matrix()[:-1], p, derivative = 2)
        strike_diff = call_data['forwardMoneyness'].as_matrix()[1:] - call_data['forwardMoneyness'].as_matrix()[:-1]
        return np.sum(weight * call_data['implPrice'].as_matrix()[:-1] * strike_diff)
    
    def E_D_p_of_R(option_data, p):
        return sum_puts(option_data, p) + sum_calls(option_data, p)
    
    def E_D_p_of_R_n(option_data, p, n):
        D_pn = E_D_p_of_R(option_data, p*n)
        D_n = E_D_p_of_R(option_data, n)
        res = (n / (p - 1)) * ((n * p - 1) * D_pn - (n - 1) * D_n)
        return res
    
    hedged_vector = []
    for moment in moment_vector:
        hedged_vector.append(E_D_p_of_R_n(option_data = options,p = p,n = moment))

    return hedged_vector

def get_appropriate_Q_Values(q_values,dates,dtm):
    # convert datastring to data pobject
    q_values['loctimestamp'] = pd.to_datetime(q_values['loctimestamp'])
    
    # Select relevant dates to search standardized 
    relevant_qs = q_values[q_values['loctimestamp'].isin(dates)]
    # Select relevante days to maturity
    relevant_qs = relevant_qs[relevant_qs['daystomaturity']==dtm]
    return relevant_qs
    
def calculate_lower_Bound(options):
    p14 = options[options['p']==1.4]['measure'].get_values()[0]
    p04 = options[options['p']==0.4]['measure'].get_values()[0]
    lower_bound = p14/p04
    return lower_bound


def get_data_from_directory(relative_path = 'name', seperator = ','):
    df = pd.read_csv(relative_path,sep = seperator)
    return df


def calc_nth_moment(x,y, order = range(1,7), centralized = False):
    #Simple function to calculate n-th moment according to 
    moment_vektor = []
    mu = np.average(x,weights = y)
    if centralized:
        if isinstance(order,range):
            moment_vektor.append(mu)
            for i in order[1:]:
                moment_vektor.append(np.average((x-mu)**i, weights = y))
            return moment_vektor
        elif isinstance(order,numbers.Number):
            return np.average((x-mu)**order,weights = y)
        else:
            raise TypeError('calc_nth_moment got a wrong argument')
    else: 
        if isinstance(order,range):
            moment_vektor.append(mu)
            for i in order[1:]:
                moment_vektor.append(np.dot(x**i,y))
            return moment_vektor
        elif isinstance(order,numbers.Number):
            return np.dot(x**order,y)
        else:
            raise TypeError('calc_nth_moment got a wrong argument')

    calc

def calc_hankel_matrix(d_J,x_i, J =3):
    hankel = np.dot(np.dot(np.vander(x_i,N = J+1, increasing = True).transpose(),np.diag(d_J)),np.vander(x_i, N = J+1, increasing = True))
    return hankel

def optimize(lower_bound, higher_bounds, qv, penalty = 10**3, vsize = 4, J = 3, d_J_bounds = (0,20), x_i_bounds = (-1,1)):
    def distance(x):
        #distance(d_J,x,p):
        #distance(x[0]-x[2],x[3]-[5],x[6])
        d_J = np.array(x[:4])
        x_i = np.array(x[4:])
        #normalize probabilites:
        d_J = d_J/sum(d_J)
        hankel = calc_hankel_matrix(d_J, x_i)
        moments = []
        moments.append(hankel[0].tolist())
        moments= moments[1:-1]
        moments.append(hankel[:][-1].tolist())
        #lower bound
        if False in (lower_bound<np.array(calc_nth_moment(x_i,d_J,order = range(1,4)))):
            return penalty
        #higher bounds
        parameter_matrix = higher_bounds.drop('daystomaturity', axis = 1)
        for row in parameter_matrix.iterrows():
            if calc_Delta_Hedged_R(calc_nth_moment(x_i,d_J,row['n'], centralized = False),row['p'])<row['measure']:
                return penalty
        
        first_factor = ((d_J - inv(np.vander(x_i,N = J+1, increasing = True).transpose())).transpose())
        diag_matrix = inv(np.diag(d_J))
        second_factor = (d_J - inv(np.vander(x_i, N = J+1,increasing = True).transpose()))
        distance_DJ = np.dot(np.dot(first_factor,diag_matrix),second_factor)

        return distance_DJ 
    
    bounds = []
    # fill in boundaries for d_j
    bounds.append(d_J_bounds*vsize)
    bounds.append(x_i_bounds*vsize)
    result = de(distance,bounds,disp = True)

    return result


if __name__ == '__main__':
    # import risk neutral probabilities
    try:
        options = get_data_from_directory('ES50_standardizedOptSet_20100826.csv',seperator = ';')
        ncc = get_data_from_directory('ES50_NCC_20100826.csv')
        ndp = get_data_from_directory('ES50_NDP_20100826.csv')
        standarizedQ = get_data_from_directory('Standardized_Qmoments.csv')
    except FileNotFoundError:
        #Additional Information if not executed in same directory
        options = get_data_from_directory('C:/Users/Alexander Jaus/Documents/Uni/Vertiefungsstudium/Seminar/Applied Risk and Asset Management/Empirical Git/ES50_standardizedOptSet_20100826.csv',seperator = ';') 
        ncc = get_data_from_directory('C:/Users/Alexander Jaus/Documents/Uni/Vertiefungsstudium/Seminar/Applied Risk and Asset Management/Empirical Git/ES50_NCC_20100826.csv')
        ndp = get_data_from_directory('C:/Users/Alexander Jaus/Documents/Uni/Vertiefungsstudium/Seminar/Applied Risk and Asset Management/Empirical Git/ES50_NDP_20100826.csv')
        standarizedQ = get_data_from_directory('C:/Users/Alexander Jaus/Documents/Uni/Vertiefungsstudium/Seminar/Applied Risk and Asset Management/Empirical Git/Standardized_Qmoments.csv', seperator = ";")
    
    # Select 30 days maturity
    options_30d = options[options['daystomaturity']==30]
    ncc_30d = ncc[ncc['daystomaturity']==30]
    ndp_30d = ndp[ndp['daystomaturity']==30]
        
    #Calculate lower bound
    lower_bound = calculate_lower_Bound(ncc_30d)
    print('The lower bound for this optimization problem is: ' + str(lower_bound))


    #result = optimize(options)
    #result.x