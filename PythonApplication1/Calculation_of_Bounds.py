import numpy as np
import pandas as pd

def f_doubleprime(R, p):
    return R**(p-2)


def EQ_DR(options,p,start,end,maturity,threshold=20):
    def discrete_sum(relevant_options,p):
        weights = f_doubleprime(relevant_options['forwardMoneyness'],p)
        # weights = weights/np.sum(weights)
        strike_diff = relevant_options['forwardMoneyness'].diff().fillna(method = 'backfill')
        prices = relevant_options['implPrice']/relevant_options['underlyingforwardprice']
        return np.sum(weights*prices*strike_diff)
        
    relevant_options = options[start:end]
    if relevant_options.empty:
        return np.NaN
    relevant_puts = relevant_options[(relevant_options['putcall']=='P') & (relevant_options['daystomaturity']==maturity) & (relevant_options['forwardMoneyness']<1)]
    if relevant_puts.empty or len(relevant_puts)<threshold:
        return np.NaN
    relevant_puts = relevant_puts.sort_values(by=['forwardMoneyness'])
    relevant_calls = relevant_options[(relevant_options['putcall']=='C') & (relevant_options['daystomaturity']==maturity) & (relevant_options['forwardMoneyness']>=1)]
    if relevant_calls.empty or len(relevant_calls)<threshold:
        return np.NaN
    relevant_calls = relevant_calls.sort_values(by=['forwardMoneyness'])
    return discrete_sum(relevant_puts,p)+discrete_sum(relevant_calls,p)
def EQ_DRN(options,p,n,start,end,maturity,threshold=20):
    return n/(p-1)*((n*p-1)*EQ_DR(options,p*n,start,end,maturity,threshold)-(n-1)*EQ_DR(options,n,start,end,maturity,threshold))
def EQ_Rp(options,p,start,end,maturity,threshold=20):
    return p*(p-1)*EQ_DR(options,p,start,end,maturity,threshold)+1

def calc_bounds(options,start_date,offset,maturity,rt='both',p=0.2,n=1.2,threshold=20):
    #handler function for EQ_DR to make calculation of bounds easier
    if isinstance(start_date,str):
        start = pd.to_datetime(start_date)
    else:
        start = start_date
    print('Optimization starts at: '+str(start))
    end = start+offset
    # The authers pose a lower bound of NCC(2/5,1)
    ncc = pd.DataFrame(columns = ['p', 'measure'])
    ncc = ncc.append({'p':float(p),'measure':EQ_Rp(options,p,start,end,maturity,threshold)},ignore_index=True)
    ncc = ncc.append({'p':float(n),'measure':EQ_Rp(options,n,start,end,maturity,threshold)},ignore_index=True)
    if ((np.isnan(ncc[ncc['p']==p]['measure'].get_values()[0])) or (np.isnan(ncc[ncc['p']==n]['measure'].get_values()[0])) or (ncc[ncc['p']==p]['measure'].get_values()[0]>3) or (ncc[ncc['p']==p]['measure'].get_values()[0]<0) or (ncc[ncc['p']==n]['measure'].get_values()[0]>3) or  (ncc[ncc['p']==n]['measure'].get_values()[0]<0)):
        ncc=None
    if rt =='ncc':
        return ncc
    # The authors pose a series of NDP bounds
    ndp = pd.DataFrame(columns = ['p', 'n', 'measure'])
    k = [2,3,4,5,6]
    n = [1,2,3,4,5,6]
    for k_index in k:
        for n_index in n:
            if k_index != n_index:
                EQ = EQ_DRN(options,k_index/n_index,n_index,start,end,maturity,threshold)
                if not np.isnan(EQ):
                    ndp = ndp.append({'p':k_index/n_index,'n':n_index,'measure':EQ},ignore_index=True)
    if rt=='ndp':
        return ndp
    return ncc,ndp


if __name__=='__main__':
    # df = pd.read_csv("ES50_standardizedOptSet_20100826.csv", delimiter = ';')
    options = pd.read_csv('FiglewskiStandardizationEOD_DE0009652396D1_standardizedOptSet.csv',sep = ';')
    options['loctimestamp']=pd.to_datetime(options['loctimestamp'])
    options.set_index('loctimestamp',inplace = True)
    options = options[options['implPrice'].notnull()&options['forwardMoneyness'].notnull()]

    start = pd.to_datetime('01-05-2008')
    end = pd.to_datetime('01-06-2008')


