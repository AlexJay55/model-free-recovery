import pandas as pd
import numpy as np
from Calculation_of_Bounds import EQ_Rp
from Calculation_of_Bounds import calc_bounds
from numpy.linalg import inv
from Moment_Recovery import calc_hankel_matrix
import statsmodels.api as sm

    
def calc_kernels(rec_matrix,options,start,end,maturity,threshold,R_J):

    result = rec_matrix[start:end]
    d_J = np.array([*result['d0'],*result['d1'],*result['d2'],*result['d3']])
    x_i = np.array([*result['x0'],*result['x1'],*result['x2'],*result['x3']])
    EQ_var = EQ_Rp(options,2,start,end,maturity,threshold)
    EQ_cub = EQ_Rp(options,3,start,end,maturity,threshold)
    EQ_RJ = np.array([1,1,EQ_var,EQ_cub])
    # R_J = np.array([1,moments['returns'],moments['variance'],moments['skewness']])
    # kernel = np.dot(np.dot(np.dot(np.dot(EQ_RJ.transpose(),inv(np.vander(x_i,N=4,increasing=True)).transpose()),inv(np.diag(d_J))),inv(np.vander(x_i,N=4,increasing=True).transpose())),R_J)
    kernel = np.dot(np.dot(EQ_RJ.transpose(),inv(calc_hankel_matrix(d_J=d_J,x_i=x_i))),R_J)
    return kernel
    
def calc_standard_moments(df, window_size='1D', start='1.1.2003', end='1.1.2014'):

    start_date = pd.to_datetime(start)
    end_date = pd.to_datetime(end)
    data = df[start_date:end_date]
    n_day_data = data.resample(window_size).first().dropna()
    n_day_return = np.log(n_day_data).diff().dropna()
    returns = np.log(data).diff().dropna()

    n = len(df[df.index < df.first_valid_index()+pd.Timedelta('1D')])
    variance = (returns**2).resample(window_size).sum().dropna()
    skewness = (np.sqrt(n)*(returns**3).resample(window_size).sum().dropna())/(variance**(3/2))
    kurtosis = (n*(returns**4).resample(window_size).sum().dropna())/(variance**2)

    moment_frame = pd.DataFrame(columns=['returns','variance','skewness','kurtosis','hyperskewness','hyperflatness'],index = n_day_return.index)
    moment_frame['returns'] = n_day_return
    moment_frame['variance'] = variance
    moment_frame['skewness'] = skewness
    moment_frame['kurtosis'] = kurtosis
    return moment_frame

def count_options(df,maturity):
    return (df[df['daystomaturity']==maturity]['daystomaturity']).resample('1D').count()

def sort_ascending_xi(ts):
    x_is = ts[['x0','x1','x2','x3']].as_matrix()
    d_is = ts[['d0','d1','d2','d3']].as_matrix()
    sort_index = np.argsort(x_is,axis=1)
    sorted_x = x_is[:, sort_index][range(x_is.shape[0]),range(x_is.shape[0]),:]
    sorted_dj = d_is[:, sort_index][range(d_is.shape[0]),range(d_is.shape[0]),:]
    ts[['x0', 'x1', 'x2', 'x3']] = sorted_x
    ts[['d0', 'd1', 'd2', 'd3']] = sorted_dj
    return ts

def prepare_ts_matrix(name, add_det=False):
    matrix = pd.read_csv(name)
    matrix = matrix.rename(columns={'Unnamed: 0':'loctimestamp'})
    matrix['loctimestamp']=pd.to_datetime(matrix['loctimestamp'])
    matrix.set_index('loctimestamp',inplace=True)
    if add_det:
        add_hank_det(matrix)
    return matrix

def add_hank_det(ts):
    ts['hank'] = ts.apply(lambda row: np.all(np.linalg.eigvals(calc_hankel_matrix(d_J=row[['d0','d1','d2','d3']],x_i=row[['x0','x1','x2','x3']]))>0),axis=1)
    ts['det'] = ts.apply(lambda row: np.linalg.det(np.array(calc_hankel_matrix(d_J=row[['d0','d1','d2','d3']],x_i=row[['x0','x1','x2','x3']]),dtype='float')),axis=1)
    return ts

def calc_hb_time_series(start,end,offset,options,maturity,threshold=20):
    def truncate(f, n):
        '''Truncates/pads a float f to n decimal places without rounding'''
        s = '{}'.format(f)
        if 'e' in s or 'E' in s:
            return '{0:.{1}f}'.format(f, n)
        i, p, d = s.partition('.')
        return '.'.join([i, (d+'0'*n)[:n]])
    if isinstance(start,str):
        start = pd.to_datetime(start,format='%d.%m.%Y')
    if isinstance(end,str):
        end = pd.to_datetime(end,format='%d.%m.%Y')
    if isinstance(offset,str):
        offset = pd.Timedelta(offset)
    index = pd.date_range(start,end,freq=offset)
    iloc = 0
    keys = calc_bounds(options,index[iloc],offset,maturity,'ndp')
    while keys.empty:
        iloc +=1 
        keys = calc_bounds(options,index[iloc],offset,maturity,'ndp')
    cols = [(truncate(col[0],3),truncate(col[1],3)) for col in zip(keys['p'],keys['n'])]

    higher_bounds = pd.DataFrame(index=index,columns=cols)
    for day in index:
        ndp = calc_bounds(options,day,offset,maturity,'ndp',threshold=20)
        if not ndp.empty:
            ndp = ndp.transpose()
            cols = [(truncate(element[0],3),truncate(element[1],3)) for element in zip(list(*ndp[ndp.index=='p'].values),list(*ndp[ndp.index=='n'].values))]
            ndp = ndp.drop(['p','n'],axis=0)
            ndp.columns = cols
            higher_bounds[day:day] = ndp.values
            #higher_bounds[day:day] = ndp.transpose()[ndp.transpose().index=='measure'].values
    return higher_bounds.dropna()

def calc_lb_time_series(options,start_date,periods,maturity,freq='30D',p=0.4,n=1.4,threshold=20):
    def calculate_lower_Bound(options):
        p14 = options[options['p']==n]['measure'].get_values()[0]
        p04 = options[options['p']==p]['measure'].get_values()[0]
        lower_bound = p14/p04
        return lower_bound
    
    dt_index = pd.date_range(start=start_date,periods=periods,freq = freq)

    lower_bounds = pd.DataFrame(columns=['LB','loctimestamp'])
    for date in dt_index:
        ncc = calc_bounds(options,date,pd.Timedelta(freq),maturity,'ncc',p=p,n=n,threshold=threshold)
        if ncc is not None:
            lb = calculate_lower_Bound(ncc)
            lower_bounds=lower_bounds.append({'LB':lb,'loctimestamp':date},ignore_index=True)
    lower_bounds.set_index('loctimestamp',inplace=True)
    return lower_bounds.dropna()

def calc_look_ahead_moments(df:pd.DataFrame, maturity ):
    # Resample the dataframe to daily data

    def var(row):
        data = log_df[row['start_time']:row['goal_time']]
        data = data.diff().dropna()
        return np.sum(data**2)

    def skew(row):
        data = log_df[row['start_time']:row['goal_time']]
        data = data.diff().dropna()
        return np.sqrt(len(data))*np.sum(data**3)/(row['variance']**(3/2))

    def kurt(row):
        data = log_df[row['start_time']:row['goal_time']]
        data = data.diff().dropna()
        return len(data) * np.sum(data ** 4) / row['variance']

    df_daily: pd.DataFrame = np.log(df.resample('1D').first().dropna())
    offset = str(maturity)+'D'
    goal_match = pd.DataFrame({'start_time':df_daily.index,'start_time_index':df_daily.index, 'goal_time':df_daily.index + pd.Timedelta(offset)})
    goal_match = goal_match.set_index('goal_time')
    goal_match['price_goal'] = df_daily['price']
    goal_match = goal_match.reset_index()
    goal_match = goal_match.set_index('start_time_index')
    goal_match['price_start'] = df_daily['price']
    goal_match = goal_match.dropna()

    # Calculate first four moments
    log_df:pd.DataFrame = np.log(df)
    goal_match['returns'] = goal_match['price_goal']-goal_match['price_start']
    goal_match['variance'] = goal_match.apply(var, axis=1)
    goal_match['skewness'] = goal_match.apply(skew, axis=1)
    goal_match['kurtosis'] = goal_match.apply(kurt, axis=1)

    return goal_match


class predicitve_regression:
    class result_holder:
        def __init__(self,ret,var,skew,kurt):
            self.ret = ret
            self.var = var
            self.skew = skew
            self.kurt = kurt

    def fit_moment(self,key_rec, key_real):
        X = self.merged[key_rec]
        Y = self.merged[key_real]
        X = sm.add_constant(X)
        reg_model = sm.OLS(Y,X)
        fit = reg_model.fit()
        return fit

    def fit_all_moments(self):
        returns = self.fit_moment('1st','returns')
        variance = self.fit_moment('2nd','variance')
        skew = self.fit_moment('3rd','skewness')
        kurt = self.fit_moment('4th', 'kurtosis')
        return self.result_holder(returns,variance,skew,kurt)

    def __init__(self,realized,recovered):
        self.realized : pd.DataFrame = realized
        self.recoverd : pd.DataFrame= recovered

        self.merged = self.recoverd.merge(self.realized,right_index=True,left_index=True)

def calc_out_of_sample_r2(ts:pd.DataFrame,look_ahead_moments:pd.DataFrame):

    def simple_ret(row):
        data = merged[row['start_time']:row['goal_time']-pd.Timedelta('1D')]
        return np.mean(data['simple_returns'])

    def simple_var(row):
        data = merged[row['start_time']:row['goal_time'] - pd.Timedelta('1D')]
        return np.var(data['simple_returns'])

    if len(ts)>len(look_ahead_moments):
        ts = ts.shift()-1
        merged = ts.merge(look_ahead_moments,right_index=True,left_index=True)
    else:
        ts = ts.shift()
        merged = look_ahead_moments.merge(ts-1,right_index=True,left_index=True)

    merged['simple_returns'] = merged['price_start'].diff().dropna()
    merged['average_returns'] = merged.apply(simple_ret,axis=1)
    merged['average_variance'] = merged.apply(simple_var,axis=1)

    SSE_ret = np.sum((merged['returns'] - merged['1st'])**2)
    SSE_mean = np.sum((merged['returns']-merged['average_returns'])**2)
    R2_ret = 1- SSE_ret/SSE_mean

    SSE_var = np.sum((merged['variance'] - merged['2nd'])**2)
    SSE_mean = np.sum((merged['variance']) - merged['average_variance']**2)
    R2_var = 1-SSE_var/SSE_mean

    return {'returs':R2_ret,'variance':R2_var}

def make_u_shape_ts(ts:pd.DataFrame,options,mat):
    def u_shape(row:pd.Series):
        m_x3 = calc_kernels(ts,options,row.name,row.name,mat,25,[1,row['x3'],row['x3']**2,row['x3']**3])
        m_1 = calc_kernels(ts,options,row.name,row.name,mat,25,[1,1,1,1])
        return (m_x3-m_1-m_1*(row['x3']-1))/(m_1*(row['x3']-1))


    ts['u-shapedness'] = ts.apply(u_shape,axis=1)
    return ts

def make_table(results,type='ret'):
    #print(make_table([pred_result_7,pred_result_30,pred_result_91,pred_result_182,pred_result_547,pred_result_730,pred_result_1095],'kurt').to_latex(float_format=lambda s:'%1.5f'%s))
    df = pd.DataFrame(columns=['alpha','p alpha = 0', 'beta','p beta = 0','p beta = 1', 'r2'], index=[7, 30, 91, 182, 547, 730, 1095])
    mapper = {'ret':'1st','var':'2nd','skew':'3rd','kurt':'4th'}
    for idx,mat in enumerate(results):
        t = getattr(mat,type)
        df.iloc[idx]['alpha'] = t.params[0]
        df.iloc[idx]['p alpha = 0'] = t.pvalues[0]
        df.iloc[idx]['beta'] = t.params[1]
        df.iloc[idx]['p beta = 0'] = t.pvalues[1]
        test = t.f_test(mapper[type]+' = 1')
        df.iloc[idx]['p beta = 1'] = test.pvalue.item()
        df.iloc[idx]['r2'] = t.rsquared
    return df







if __name__=='__main__':

    raw_data = pd.read_csv('eurostoxx50_prices_5m.csv', sep =';')
    raw_data['loctimestamp'] = pd.to_datetime(raw_data['loctimestamp'])
    raw_data.set_index('loctimestamp', inplace = True)
    start_date = pd.Timestamp(2002,1,1)
    end_date = pd.Timestamp(2016,1,1)
    raw_data = raw_data[start_date:end_date]

    options = pd.read_csv('FiglewskiStandardizationEOD_DE0009652396D1_standardizedOptSet.csv',sep = ';')
    options['loctimestamp']=pd.to_datetime(options['loctimestamp'])
    options.set_index('loctimestamp',inplace = True)
    options = options[options['implPrice'].notnull()&options['forwardMoneyness'].notnull()]


    look_ahead_moments_7 = calc_look_ahead_moments(raw_data,7)
    look_ahead_moments_30 = calc_look_ahead_moments(raw_data,30)
    look_ahead_moments_91 = calc_look_ahead_moments(raw_data,91)
    look_ahead_moments_182 = calc_look_ahead_moments(raw_data,182)
    look_ahead_moments_547 = calc_look_ahead_moments(raw_data,547)
    look_ahead_moments_730 = calc_look_ahead_moments(raw_data,730)
    look_ahead_moments_1095 = calc_look_ahead_moments(raw_data,1095)
    ts_7 = prepare_ts_matrix('rec_7.csv')
    ts_30 = prepare_ts_matrix('rec_30.csv')
    ts_91 = prepare_ts_matrix('rec_91.csv')
    ts_182 = prepare_ts_matrix('rec_182.csv')
    ts_547 = prepare_ts_matrix('rec_547.csv')
    ts_730 = prepare_ts_matrix('rec_730.csv')
    ts_1095 = prepare_ts_matrix('rec_1095.csv')

    # pred_model_7 = predicitve_regression(look_ahead_moments_7,ts_7-1)
    # pred_result_7 = pred_model_7.fit_all_moments()
    # pred_model_30 = predicitve_regression(look_ahead_moments_30,ts_30-1)
    # pred_result_30 = pred_model_30.fit_all_moments()
    # pred_model_91 = predicitve_regression(look_ahead_moments_91,ts_91-1)
    # pred_result_91 = pred_model_91.fit_all_moments()
    # pred_model_182 = predicitve_regression(look_ahead_moments_182,ts_182-1)
    # pred_result_182 = pred_model_182.fit_all_moments()
    # pred_model_547 = predicitve_regression(look_ahead_moments_547,ts_547-1)
    # pred_result_547 = pred_model_547.fit_all_moments()
    # pred_model_730 = predicitve_regression(look_ahead_moments_730,ts_730-1)
    # pred_result_730 = pred_model_730.fit_all_moments()
    # pred_model_1095 = predicitve_regression(look_ahead_moments_1095,ts_1095-1)
    # pred_result_1095 = pred_model_1095.fit_all_moments()

