import matplotlib.pyplot as plt
from matplotlib import style
style.use('ggplot')
from scipy import stats
import pandas as pd
import numpy as np
import seaborn as sns
from ast import literal_eval as make_tuple
from Moment_Recovery import calc_Delta_Hedged_R
from TS_Analysis import sort_ascending_xi
from TS_Analysis import calc_kernels
from TS_Analysis import predicitve_regression
import statsmodels.api as sm


def plot_higher_bounds(hb, moment, plot_recovered_moments=False, rec_moments=None, ):
    if plot_recovered_moments and (rec_moments is None):
        raise RuntimeError('Plotting moments requires recovered time series')
    moment_str = '{:.3f}'.format(moment)
    for key in hb.keys():
        key = make_tuple(key)
        if key[1] == moment_str:
            plt.plot(hb[str(key)], label=str(key[0]))
    if plot_recovered_moments:
        mapper = {1: '1st', 2: '2nd', 3: '3rd', 4: '4th', 5: '5th', 6: '6th'}
        for key2 in hb.keys():
            key2 = make_tuple(key2)
            if key2[1] == moment_str:
                Delta_hedged_R = calc_Delta_Hedged_R(rec_moments[mapper[int(float(make_tuple(key2)[1]))]],
                                                     float(make_tuple(key2)[0]))
                plt.plot(Delta_hedged_R, label='DH' + str(key2[0]))
    plt.legend()


def plot_lower_bounds(series, title):
    '''Takes a series of lower bounds displayed as a dict with keys {values: , mat: }'''
    fig = plt.figure()
    for element in series:
        plt.plot(element['values'], label='Mat: ' + str(element['mat']))
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    # plt.title(title)
    # plt.show()


def plot_cond_moment(con_moment, drop_outlayers=True):
    if drop_outlayers:
        transformed_test_data = con_moment[(np.abs(stats.zscore(con_moment)) < 3)]
        plt.plot(transformed_test_data)
    else:
        plt.plot(con_moment)
    plt.title('Recovered Conditional Forward Moments')
    plt.show()


def plot_lower_bounds(series, title):
    fig = plt.figure()
    for element in series:
        plt.plot(element['values'], label='Mat: ' + str(element['mat']))
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
          fancybox=True, shadow=True, ncol=5)
    plt.title(title)
    plt.show()


def plot_kernels(date, options, ts, mat):
    if isinstance(date, str):
        date = pd.to_datetime(date, format='%d.%m.%Y')
    try:
        result = ts[date:date]
        xi = result[['x0', 'x1', 'x2', 'x3']]
        x_min = np.min(xi, axis=1)
        x_max = np.max(xi, axis=1)
        vals = []
        x = np.linspace(x_min, x_max, 50)
        for r in x:
            vals.append(calc_kernels(ts, options, date, date, mat, 25, [1, r, r ** 2, r ** 3]))
        plt.plot(x, vals, label=date.strftime('%d.%m.%Y') + ' mat:' + str(mat))
        #plt.axvline(x=x_min.item())
        #plt.axvline(x=x_max.item())
        #plt.legend()
        plt.show()
    except np.linalg.LinAlgError:
        return plot_kernels(date + pd.Timedelta('1D'), options, ts, mat)


def plot_option_analysis(options, maturities, type):
    if maturities not in options['daystomaturity'].unique():
        raise ValueError
    options_mat = dict(zip(options['daystomaturity'].unique(),
                           [options[options['daystomaturity'] == mat] for mat in options['daystomaturity'].unique()]))
    # Plot Number of Derivatives
    if type == 'count':
        f, ax_list = plt.subplots(len(options['daystomaturity'].unique()), sharex=True)
        for key, ax in zip(options_mat.keys(), ax_list):
            ts = options_mat[key]['daystomaturity'].resample('1D').count()
            ts = ts.where(ts != 0).dropna()
            ax.plot(ts, label=str(key))
            ax.legend(loc=3)
        plt.show()
    if type == 'max':
        f, ax_list = plt.subplots(len(options['daystomaturity'].unique()), sharex=True)
        for key, ax in zip(options_mat.keys(), ax_list):
            ts = options_mat[key]['forwardMoneyness'].resample('1D').max()
            ts = ts.where(ts != 0).dropna()
            ax.plot(ts, label=str(key))
            ax.legend(loc=3)
        plt.show()
    if type == 'min':
        f, ax_list = plt.subplots(len(options['daystomaturity'].unique()), sharex=True)
        for key, ax in zip(options_mat.keys(), ax_list):
            ts = options_mat[key]['forwardMoneyness'].resample('1D').min()
            ts = ts.where(ts != 0).dropna()
            ax.plot(ts, label=str(key))
            ax.legend(loc=3)
        plt.show()
    if type == 'minmax':
        f, ax_list = plt.subplots(len(options['daystomaturity'].unique()), sharex=True)
        for key, ax in zip(options_mat.keys(), ax_list):
            ts_min = options_mat[key]['forwardMoneyness'].resample('1D').min()
            ts_min = ts_min.where(ts_min != 0).dropna()
            ax.plot(ts_min, label=str(key),color = 'r')
            ts_max = options_mat[key]['forwardMoneyness'].resample('1D').max()
            ts_max = ts_max.where(ts_max != 0).dropna()
            ax.plot(ts_max, label=str(key), color='b')
            ax.legend(loc=3)
        plt.show()


def plot_predicted_and_real(pred, real, ret_object=False,moment=1):
    moment_mapper_pred = {1:"1st",2:"2nd",3:"3rd",4:"4th"}
    moment_mapper_real = {1:"returns",2:"variance",3:"skewness",4:"kurtosis"}

    fig, ax1 = plt.subplots()
    ax1.set_ylabel('Recovered Moments',color='b')
    l1 = ax1.plot(pred[moment_mapper_pred[moment]], color='b', label="Prediction")
    ax2 = ax1.twinx()
    l2 = ax2.plot(real[moment_mapper_real[moment]], color='r', label="Realization")
    ax2.set_ylabel('Realized Moments',color='r')
    plt.title(moment_mapper_real[moment])
    lns = l1+l2
    labs = [l.get_label() for l in lns]
    leg = ax1.legend(lns, labs, loc=0)
    for text in leg.get_texts():
        text.set_color('black')
    if not ret_object:
        plt.show()

def plot_scatter_pred_real(pred,real,moment, ret_object=False):
    moment_mapper_pred = {1:"1st",2:"2nd",3:"3rd",4:"4th"}
    moment_mapper_real = {1:"returns",2:"variance",3:"skewness",4:"kurtosis"}
    df = pd.merge(pred, real, left_index=True, right_index=True, how='inner')
    plt.scatter(df[moment_mapper_real[moment]],df[moment_mapper_pred[moment]])
    plt.xlabel("Real Moment")
    plt.ylabel("Predicted Moment")
    plt.legend()
    if not ret_object:
        plt.show()

def plot_first_four_moment(ts,labels=None):
    if type(ts) is pd.DataFrame:
        fig, axis = plt.subplots(2,2)
        axis[0,0].plot(ts['1st'])
        axis[0,0].set_title('Returns')
        axis[0,1].plot(ts['2nd'])
        axis[0,1].set_title('Variance')
        axis[1,0].plot(ts['3rd'])
        axis[1,0].set_title('Skewness')
        axis[1,1].plot(ts['4th'])
        axis[1,1].set_title('Kurtosis')
    elif type(ts) is list:
        fig, axis = plt.subplots(2, 2)
        for ind, series in enumerate(ts):
            axis[0, 0].plot(series['1st'],label=labels[ind])
            plt.legend()
            axis[0, 1].plot(series['2nd'],label=labels[ind])
            plt.legend()
            axis[1, 0].plot(series['3rd'],label=labels[ind])
            plt.legend()
            axis[1, 1].plot(series['4th'],label=labels[ind])
            plt.legend()

        axis[0, 0].set_title('Returns')
        axis[0, 1].set_title('Variance')
        axis[1, 0].set_title('Skewness')
        axis[1, 1].set_title('Kurtosis')


    else:
        raise ValueError('Passed Unknown Type to plot function')

def plot_options_and_support_space(options, ts, maturity, returns, type='count'):
    ts_asc = sort_ascending_xi(ts)
    if type == 'count':
        opt_ts = options[options['daystomaturity'] == maturity]['daystomaturity'].resample('1D').count()
        opt_ts = opt_ts.where(opt_ts != 0).dropna()

        fig, ax1 = plt.subplots()
        ax1.plot(opt_ts, label='No of options')
        ax2 = ax1.twinx()
        ax2.plot(ts_asc['x0'], 'b', label='x0')
        ax1.legend()
        ax2.legend()
        plt.show()
    if type == 'min' or type == 'max':
        if type == 'min':
            opt_ts = options[options['daystomaturity'] == maturity]['forwardMoneyness'].resample('1D').min()
        else:
            opt_ts = options[options['daystomaturity'] == maturity]['forwardMoneyness'].resample('1D').max()
        opt_ts = opt_ts.where(opt_ts != 0).dropna()
        fig, ax1 = plt.subplots()
        ax1.plot(opt_ts, label=type + ' of option strike')
        ax2 = ax1.twinx()
        ax2.plot(ts_asc['x0'], 'b', label='x0')
        ax1.legend()
        ax2.legend()
        plt.show()
    if type == 'minmax':
        opt_ts_min = options[options['daystomaturity'] == maturity]['forwardMoneyness'].resample('1D').min()
        opt_ts_max = options[options['daystomaturity'] == maturity]['forwardMoneyness'].resample('1D').max()
        plt.plot(ts_asc['x0'] - 1, label='x0')
        plt.plot(ts_asc['x3'] - 1, 'y', label='x3')
        plt.plot(opt_ts_min - 1, 'b', label='min strike')
        plt.plot(opt_ts_max - 1, 'g', label='max strike')
        plt.plot(returns['returns'], 'c', label='returns')
        plt.legend()
        plt.show()



def plot_regression_results_for_mat(look_ahead:pd.DataFrame, time_series:pd.DataFrame, **kwargs):

    if len(look_ahead)>len(time_series):
        merged = look_ahead.merge(time_series,left_index=True,right_index=True)
    else:
        merged = time_series.merge(look_ahead,right_index=True,left_index=True)
    fig, axis = plt.subplots(2,2)
    sns.regplot('1st','returns', merged, ax=axis[0,0],**kwargs)
    axis[0, 0].set_title('Returns')
    axis[0, 0].set(xlabel='Recovered', ylabel='Realized')
    sns.regplot('2nd','variance', merged, ax=axis[0,1],**kwargs)
    axis[0, 1].set_title('Variance')
    axis[0, 1].set(xlabel='Recovered', ylabel='Realized')
    sns.regplot('3rd', 'skewness', merged, ax=axis[1,0],**kwargs)
    axis[1, 0].set_title('Skewness')
    axis[1, 0].set(xlabel='Recovered', ylabel='Realized',**kwargs)
    sns.regplot('4th','kurtosis',merged, ax=axis[1,1])
    axis[1, 1].set_title('Kurtosis')
    axis[1, 1].set(xlabel='Recovered', ylabel='Realized')


def plt_moment_all_mat(tseries,moments,moment):
    moment_mapper_ts = {1:'1st',2:'2nd',3:'3rd',4:'4th'}
    moment_mapper_mom = {1:'returns',2:'variance',3:'skewness',4:'kurtosis'}
    merged_7 = tseries[0].merge(moments[0],left_index=True,right_index=True)
    merged_30 = tseries[1].merge(moments[1],left_index=True,right_index=True)
    merged_91 = tseries[2].merge(moments[2],left_index=True,right_index=True)
    merged_182 = tseries[3].merge(moments[3],left_index=True,right_index=True)
    merged_547 = tseries[4].merge(moments[4],left_index=True,right_index=True)
    merged_730 = tseries[5].merge(moments[5],left_index=True,right_index=True)
    merged_1095 = tseries[6].merge(moments[6],left_index=True,right_index=True)

    fig, axis = plt.subplots(4,2)
    sns.regplot(moment_mapper_ts[moment],moment_mapper_mom[moment],merged_7,ax=axis[0,0])
    sns.regplot(moment_mapper_ts[moment], moment_mapper_mom[moment], merged_30, ax=axis[0, 1])
    sns.regplot(moment_mapper_ts[moment], moment_mapper_mom[moment], merged_91, ax=axis[1, 0])
    sns.regplot(moment_mapper_ts[moment], moment_mapper_mom[moment], merged_182, ax=axis[1, 1])
    sns.regplot(moment_mapper_ts[moment], moment_mapper_mom[moment], merged_547, ax=axis[2, 0])
    sns.regplot(moment_mapper_ts[moment], moment_mapper_mom[moment], merged_730, ax=axis[2, 1])
    sns.regplot(moment_mapper_ts[moment], moment_mapper_mom[moment], merged_1095, ax=axis[3, 0])
    axis[0, 0].set(xlabel='Rec 7', ylabel='Realized')
    axis[0, 1].set(xlabel='Rec 30', ylabel='Realized')
    axis[1, 0].set(xlabel='Rec 91', ylabel='Realized')
    axis[1, 1].set(xlabel='Rec 182', ylabel='Realized')
    axis[2, 0].set(xlabel='Rec 547', ylabel='Realized')
    axis[2, 1].set(xlabel='Rec 730', ylabel='Realized')
    axis[3, 0].set(xlabel='Rec 1095', ylabel='Realized')