import numpy as np
from numpy.linalg import inv
import pandas as pd
from matplotlib import style
style.use('ggplot')
from scipy.optimize import differential_evolution as de
from Calculation_of_Bounds import calc_bounds
from Calculation_of_Bounds import EQ_Rp
import numbers


def calc_Delta_Hedged_R(R,p):
    def f_R(R,p,derivative = 0):
    #convex payoff function and derivatives

        if derivative == 0:
            return (R**p-1)/(p*(p-1))
        if derivative == 1:
            return (R**(p-1))/(p-1)
        if derivative == 2:
            return R**(p-2)
    DfR = f_R(R,p)-f_R(1,p)-f_R(1,p,derivative = 1)*(R-1)
    return DfR

def get_appropriate_Q_Values_from_xls(q_values,risk_free_R,date,dtm):
    # Select relevant dates to search standardized 
    try:
        relevant_qs = q_values.loc[date.normalize()]
        relevant_r = risk_free_R.loc[date.normalize()]
    except KeyError:
        return None
    # Select relevante days to maturity
    relevant_qs = relevant_qs[relevant_qs['daystomaturity']==dtm]
    relevant_r = relevant_r[relevant_r['daystomaturity']==dtm]
    if relevant_qs.empty or relevant_r.empty:
        return None
    relevant_qs['Q_expected'] = float(relevant_r['normalizedR'])
    return relevant_qs

def calc_appropriate_Q_Values(options,start,offset,maturity):
    start_date = pd.to_datetime(start)
    end_date = start_date+offset
    EQ_R_var = EQ_Rp(options,2,start_date,end_date,maturity)
    EQ_R_cubic = EQ_Rp(options,3,start,end_date,maturity)
    if np.isnan(EQ_R_var) or np.isnan(EQ_R_cubic):
        return None
    return {'Q_variance':EQ_R_var,'Q_cubic':EQ_R_cubic}

def calculate_lower_Bound(ncc,p=0.6,n=0.1):
    p14 = ncc[ncc['p']==n]['measure'].get_values()[0]
    p04 = ncc[ncc['p']==p]['measure'].get_values()[0]
    lower_bound = p14/p04
    return lower_bound


def get_data_from_directory(relative_path = 'name', seperator = ','):
    df = pd.read_csv(relative_path,sep = seperator)
    return df


def calc_nth_moment(x,y, order = range(1,7), centralized = False):
    #Simple function to calculate n-th standard moment
    moment_vektor = []
    mu = np.average(x,weights = y)
    if centralized:
        if isinstance(order,range):
            moment_vektor.append(mu)
            for i in order[1:]:
                moment_vektor.append(np.average((x-mu)**i, weights = y))
            return moment_vektor
        elif isinstance(order,numbers.Number):
            return np.average((x-mu)**order,weights = y)
        else:
            raise TypeError('calc_nth_moment got a wrong argument')
    else: 
        if isinstance(order,range):
            moment_vektor.append(mu)
            for i in order[1:]:
                moment_vektor.append(np.dot(x**i,y))
            return moment_vektor
        elif isinstance(order,numbers.Number):
            return np.dot(x**order,y)
        else:
            raise TypeError('calc_nth_moment got a wrong argument')



def calc_hankel_matrix(d_J = None, x_i = None, result = None, J = 3):
    #Calculates Hankel Matrix from row of recovered time series
    if result is not None:
        d_J = result.x[:4]
        x_i = result.x[4:]
    hankel = np.dot(np.dot(np.vander(x_i,N = J+1, increasing = True).transpose(),np.diag(d_J)),np.vander(x_i, N = J+1, increasing = True))
    return hankel

def calc_recovered_time_series(options, start, periods, freq='1D', maturity =30, p = 0.4, n = 1.4,threshold=25,hankel_sum=False,expicit_DJ=False,increasing_xi=False):
    #Calculates the main optimization problem for a time series
    index = pd.date_range(start,periods=periods, freq = freq)
    hankel_frame = pd.DataFrame(data=np.nan,index=index,columns=['1st','2nd','3rd','4th','5th','6th','ZFW','d0','d1','d2','d3','x0','x1','x2','x3'])
    for date in index:

        ncc,ndp = calc_bounds(options,date,offset=pd.Timedelta(freq),maturity=maturity, p=p,n=n,threshold=25)
        if (ncc is not None):
            lower_bound = calculate_lower_Bound(ncc,p=p,n=n)
            #lower_bound = 1.002
            print('The lower Bound for the optimization Problem is '+str(lower_bound))
            #qVales = get_appropriate_Q_Values_from_xls(standardizedQ,risk_free_R,date,maturity)
            qVales = calc_appropriate_Q_Values(options,date,pd.Timedelta(freq),maturity)
            print(qVales)
            if qVales is not None:
                result = optimize(lower_bound,ndp,qVales,1000000,hankel_sum=hankel_sum,expicit_DJ=expicit_DJ,increasing_xi=increasing_xi)
                hankel = calc_hankel_matrix(result=result)
                moments = hankel[0].tolist()
                moments= moments[1:-1]
                moments.extend(hankel[:][-1].tolist())
            
                hankel_frame.loc[date]['1st'] = moments[0]
                hankel_frame.loc[date]['2nd'] = moments[1]
                hankel_frame.loc[date]['3rd'] = moments[2]
                hankel_frame.loc[date]['4th'] = moments[3]
                hankel_frame.loc[date]['5th'] = moments[4]
                hankel_frame.loc[date]['6th'] = moments[5]
                hankel_frame.loc[date]['ZFW'] = result.fun
                hankel_frame.loc[date]['d0'] = result.x[0]
                hankel_frame.loc[date]['d1'] = result.x[1]
                hankel_frame.loc[date]['d2'] = result.x[2]
                hankel_frame.loc[date]['d3'] = result.x[3]
                hankel_frame.loc[date]['x0'] = result.x[4]
                hankel_frame.loc[date]['x1'] = result.x[5]
                hankel_frame.loc[date]['x2'] = result.x[6]
                hankel_frame.loc[date]['x3'] = result.x[7]
        print('Optimization is finished: '+str(date))
    return hankel_frame.dropna()


def optimize(ncc,ndp,qv,t,d_J_bounds = (0.0,1), x_i_bounds = (0.001,4),penalty = 10**6):
        #main algorithm to compute the minimization problem
        def distance(x):
            d_J = np.array(x[:4])
            x_i = np.array(x[4:])
            eq = np.array([1,1,qv['Q_variance'],qv['Q_cubic']])
            try:
                first_factor = (d_J - np.dot(inv(np.vander(x_i,N = 4, increasing = True).transpose()),eq)).transpose()
                diag_matrix = inv(np.diag(d_J))
                second_factor = (d_J - np.dot(inv(np.vander(x_i, N = 4,increasing = True).transpose()),eq))
            except np.linalg.LinAlgError:
                #print('Linal Error Term 3')
                return penalty+np.random.normal(0,10)
            distance_DJ = np.dot(np.dot(first_factor,diag_matrix),second_factor)
            return distance_DJ
        def punishment(x):
            terms = []
            d_J = np.array(x[:4])
            #d_J = d_J/sum(d_J)
            x_i = np.array(x[4:])
            #d_J = np.append(d_J,1-np.sum(d_J))
            
            #Add lower bounds
            hankel = calc_hankel_matrix(d_J=d_J, x_i= x_i)
            moments = hankel[0].tolist()
            moments= moments[1:-1]
            moments.extend(hankel[:][-1].tolist())
            terms.append(np.max([0,ncc-moments[0]])**2)
            
            # Add higher bounds
            for index,row in ndp.iterrows():
                delta_hedged_R = calc_Delta_Hedged_R(moments[int(row['n'])-1],row['p'])
                if not np.isnan(delta_hedged_R):
                    terms.append(np.max([0,delta_hedged_R-row['measure']])**2)
            
            # Add constraint that sum of d_j must equal = 1
            terms.append(np.max([0,sum(d_J)-1])**2)
            terms.append(np.max([0,1-sum(d_J)])**2)

            punish = np.sum(terms)
            return punish
        
        def obj_function(x):
            return distance(x)+t*punishment(x)

        bounds = []

        bounds.extend([d_J_bounds]*4)
        bounds.extend([x_i_bounds]*4)

        result = de(obj_function,bounds,disp = True,tol=0.001,polish= True,maxiter=10000)
        return result




if __name__ == '__main__':
    # import risk neutral probabilities

    options = get_data_from_directory('FiglewskiStandardizationEOD_DE0009652396D1_standardizedOptSet.csv',seperator = ';')
    options['loctimestamp'] = pd.to_datetime(options['loctimestamp'])
    options.set_index('loctimestamp',inplace = True)
    options = options[options['implPrice'].notnull()&options['forwardMoneyness'].notnull()]
    
    for maturity in options['daystomaturity'].unique():
        recovered_matrix = calc_recovered_time_series(options,'01-01-2008',365,maturity=maturity,freq='1D',hankel_sum=True,expicit_DJ=False,increasing_xi=True)
        name = 'rec_'+str(maturity)+'.csv'
        recovered_matrix.to_csv(name)

