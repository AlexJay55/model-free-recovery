%% ==============
\chapter{Application Analysis}
\label{ch:Empirical Findings}
%% ==============
\section{Data Description}
\label{Sec: Data Description}
We rely on a standardized option set containing daily prices of various Puts and Calls written on the EURO STOXX 50, a stock index being an indicator for the 50 largest and most liquid stocks of the euro zone.
Since the computation of the expected values under the risk neutral probability $\mathbb{Q}$ strongly depends on our available options as well as the strike range of the options on a certain day, it is worth looking into these properties before proceeding. 
As it is displayed in figure \ref{Fig: Option Count}, the amount of available options varies quite strongly depending both on macroeconomic factors such as crises as well as repetitive noise patterns. In order to compensate some of this noise, we will only regard days on which the number of available options exceeds a certain threshold, which in our case is set to 25 options. 
\begin{figure}[h]
	\includegraphics[width=\textwidth]{option_count}
	\caption{Number of daily options for all available maturities}
	\label{Fig: Option Count}
\end{figure}
Figure \ref{Fig: Option Minmax} reports the available range between minimum and maximum strikes each day. Again there seem to be the previously mentioned drivers of the noise. It also is worth mentioning that the general patterns from figure \ref{Fig: Option Count} and \ref{Fig: Option Minmax} are quite similar. 
Regarding the general noise patterns we might expect similar noise patterns in our results. 
\begin{figure}[h]
	\includegraphics[width=\textwidth]{option_minmax}
	\caption{Minimum (red) and maximum (blue) option strikes available on each day within the regarded time period. Option strikes are measured in forward moneyness.}
		\label{Fig: Option Minmax}
\end{figure}

\section{Implementation}
\label{Sec: Implementation}
This section shows how the main optimization problem \ref{Eq: Final Optimization Problem} was implemented using the programming language Python 3.6. Since the entire optimization problem is not convex, we followed the suggestions of the authors and used a differential evolution algorithm in order to minimize the HJ Distance.

We faced the problem that the differential evolution algorithm, as it is available in the scipy library, was not suitable for solving constrained optimization problems. In order to still be able to work with this well tested scipy algorithm, we transformed our constrained optimization problem into an unconstrained problem by using a standard penalty method approach.

The main idea constitutes to punish not admissible points instead of simply not to allow them.  Let the following setup be our constrained optimization problem $P$:
\begin{equation}
P: \min_x\ f(x)\ s.t.\ g_i(x)\leq0, \ i\in I
\label{Eq: Optimization Problem}
\end{equation}
We then can construct a penalty term $\alpha (x)$
\begin{equation}
\alpha(x) = \sum_{i\in I} max\{0,g_i(x)\}^2
\label{Eq: Penalty Term}
\end{equation}
with the following properties: 
\begin{itemize}
	\item $\forall x \in \mathbb{R}^n: \alpha(x)\geq 0$
	\item $\alpha(x) = 0 \Leftrightarrow x\ is\ admissible$
	\item $\alpha$ is a continuously differentiable function which can be interpreted as a measure for admissibility
\end{itemize}
We now combine the two goals of minimizing $P$ and minimizing $\alpha$ to the following combined aim with weight $t>0$:
\begin{equation}
P(t): \min_x\ f(x)+t*\alpha(x)
\label{Eq: Tranformed Optimization Problem}
\end{equation}
It can be shown that $P(t)$ converges with growing $t$ to $P$. This implies that if we choose $t$ large enough, it is sufficient to solve the transformed problem in order to sort out the constrained problem. In order to identify appropriate values for $t$, we increased $t$ on a test sample until most of the $\alpha(x)=0$. In this implementation, we set $t=10^6$. 

Using the transformed problem for every day within the regarded period of time, we minimize the HJ distance with the differential evolution algorithm of the scipy library. It is crucial to understand that a differential evolution algorithm is an evolutionary algorithm which iteratively improves the solution based on a population model. However we can by no means guarantee the optimality of a calculated solution due to the fact that we might either be caught in a local minimum or not have approached the global optimal close enough. Nevertheless, the differential evolution algorithm depicts a powerful tool which yields extremely good results in the vast majority of the cases. 

First we calculate the lower bound for each day as well as the series of upper bounds as it has been described by \cite{Schneider2017} within chapter \ref{ch:Theory}. An overview of the time series of lower bounds and upper bounds for different maturities can be seen in figure \ref{Fig: Lower Bounds} and figure \ref{Fig: Higher Bounds} respectively.

Our daily optimization problem is constrained by an $NCC(0.4, 1.4)$ lower bound constraint, restricting the first moment, and an $NDP(\frac{k}{n}, n)$ with $k=2,3,\dots,6$ and $n=1,2,\dots,6$ higher bound constraints. Both lower and higher bounds are added into the optimization problem. We furthermore add an equality constraint which constitutes that all probabilities $d_J$ need to sum up to $1$.

Considering the final optimization problem as stated in equation: \ref{Eq: Final Optimization Problem}, the  coherence can be derived that the HJ distance depends on the probability vector $d_J$. However we also need to contemplate the discrete state space $\{x_0,\dots,x_J\} \subset (0,\inf)$ on which the probabilities $d_J$ are defined. Since the authors assume the discrete state space as well as $d_J$ to be free parameters, we also optimize with respect to those. Our results thus include a time series of probability vectors and their corresponding state space parameters. From the parameters we then calculate the moments by constructing the Hankel Matrix according to the Vandermonde decomposition $H_0 = VDV'$.



\chapter{Results and Discussion}
\label{Ch: Results}

This section contains an overview of our results. We first examine the calculated upper and lower bounds before having a closer look at the recovered conditional moments. 
\section{Bounds}
\label{Sec: Bounds}
As already stated in chapter \ref{ch:Theory}, bounds are necessary in order to obtain an informative solution. Without any bounds, the optimization algorithm for the HJ distance would simply lead a result of zero.
Figure \ref{Fig: Lower Bounds} reports the lower bounds for all available maturities. It is evident that all of the lower bounds follow a similar pattern. Although they are quite low during calm financially stable times, in times of financial distress the lower bound for returns increases quite dramatically. It furthermore can be observed that constraints for shorter maturities lay almost always below the constraints for longer maturities. This seems quite straightforward since most investors would expect longer maturities to yield higher returns. 

Another important observation is that maturities 730 and 912 do have a lot of noise around 2008 and 2009. This needs to be perceived when examining the recovered conditional forward moments since these directly depend on the lower bounds. One possible cause for this high amount of noise within the two states of maturity can be found when examining figure \ref{Fig: Option Count} as well as figure \ref{Fig: Option Minmax}: Shortly after 2008 the number of options as well as the available minimum and maximum strike for maturity 730 show some strong noise pattern which can be found at our lower bounds at the exact same time. Similar conclusions can also be drawn for maturity 912.

\begin{figure}
	\includegraphics[width=1.0\textwidth]{LBS}
	\caption{Time series of lower bounds NCC(0.4, 1.0) for all available maturities. The plotted time series constrain the first moment of return.}
		\label{Fig: Lower Bounds}
\end{figure}

Figure \ref{Fig: Higher Bounds} displays an extraction of the set of all 25 upper bounds. The higher bounds are also increasing during financially troublesome time and decrease during calm stable times. However it is important to keep in mind that the higher bounds do not directly constrain moments but rather the delta hedged payoff $\mathcal{D}^p(\mathbb{E}^\mathbb{P}_t(R^n))$ of the moments.

\begin{figure}
	\includegraphics[width=\textwidth]{HBS}
	\label{Fig: Higher Bounds}
	\caption{Time series of six of the set of the 25 $NDP(\frac{k}{n}, n)$ higher bounds for maturity 91 days. The plotted time series constrain \textsuperscript{th} moment of return.}
\end{figure}

\section{Recovered Moments}
\label{Sec: Results}
\begin{figure}
	\includegraphics[width=\textwidth]{Rec_Moments_91}
	\caption{First four recovered conditional forward moments of return for 91 days maturity.}
	\label{Fig: Rec Moments 91}
\end{figure}
As a result of optimization problem \ref{Eq: Tranformed Optimization Problem}, we receive the optimal probability vector $d_J$ and the corresponding state space $\{x_0,\dots,x_J\}$ with $J=3$ since we, as previously described, merely regard minimum variance projections of order 3. 
Recovered moments can thus be directly extracted from the Hankel matrix which is constructed from the Vandermonde factorization. 
An overview of the first four recovered moments from the minimum variance projection of order $J=3$ for the maturity of 91 days is shown in figure \ref{Fig: Rec Moments 91}. An interesting observation is that the first moment is mostly determined by the pattern of the lower bounds. However also higher moments follow this pattern even though they are not directly constrained by the lower bound. \cite{Schneider2017} argued that higher moments are restricted as well since they are supported by the same probability distribution implied by the Vandermonde decomposition. 

Figure \ref{Fig: Rec Moments multiple} displays a comparison of recovered returns from various maturities. We can ascertain that apart from a few outliers longer, recovered moments from shorter maturities are smaller than returns based on longer maturities. This finding is consistent with the findings from the lower bounds. Another relevant aspect is that the noisy pattern of the maturity 730 can also be found in all of the recovered moments. This noise is directly incorporated by the noise in the lower bound for maturity 730 days.
\begin{figure}
	\includegraphics[width=\textwidth]{Rec_Moments_30_91_182_730}
	\caption{First four recovered conditional forward moments of return for 30, 91, 182 and 730 days maturity.}
	\label{Fig: Rec Moments multiple}
\end{figure}
Another finding is that with growing maturities, the random noise in the recovered moments becomes less dominant whereas macroeconomic effects such as crises are responsible for the major changes within the time series.

\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{Rec_first_moments}
	\label{Fig: Rec_first_moments}
	\caption{First recovered moments for maturities 7, 30 and 182 days}
\end{figure}

\section{Predictive Regression}
\label{Sec: Results}

Since our moments are recovered from derivatives with a certain maturity in $T$, their prices do contain market beliefs about the state of the market at time $T$. The natural question thus arises whether the recovered returns from these derivative prices do have any predicting power for future market realizations. We therefore conducted a predictive regression in which we estimate the following model via OLS:
\begin{equation}
m_{t,\ t\rightarrow T} = \alpha + \beta*\mathbb{E}_t[m_{t\rightarrow T}]+\epsilon_t
\label{EQ: Predicitive Regression}
\end{equation}
where $m_{t, t\rightarrow T}$ is the realized moment of return between time $t$ and $T$ at time $t$. With $T-t$ being equal to the maturity of the derivatives used to recover conditional forward moments, we regress the recovered moment of return which can be seen as $\mathbb{E}_t[m_{t\rightarrow T]}$ on the actual realized forward moments. 

In order to compare our recovered moments with the realized moments, we first compute the forward looking moments for time $t$ with a $T-t$ horizon. Furthermore we shift the recovered moments by $-1$ in order to compare simple excess returns. 

After running this regression on all maturities for the first moment of return, we receive the following results for the estimators $\alpha$ and $\beta$:

\begin{table}[h]
	\centering
\begin{tabular}{l|ll|ll|l}
	\toprule
	{maturity} &    $\alpha$ & P: $\alpha$ = 0 &     $\beta$ & P: $\beta$ = 0 &      $R^2$ \\
	\midrule
	7    & -0.00123 &     0.12835 & -0.08843 &    0.70393 & 0.00007 \\
	30   &  0.00164 &     0.48088 & -1.35128 &    0.02124 & 0.00349 \\
	91   &  0.00037 &     0.89727 & -0.34261 &    0.24531 & 0.00046 \\
	182  & -0.01900 &     0.00034 &  1.39129 &    0.00004 & 0.00558 \\
	547  &  0.01913 &     0.00256 &  0.43198 &    0.00000 & 0.01022 \\
	730  &  0.06417 &     0.00000 & -0.15017 &    0.15594 & 0.00124 \\
	1095 & -0.14133 &     0.00000 &  3.31890 &    0.00000 & 0.08790 \\
	\bottomrule
\end{tabular}
	\caption{Predictive Regression for first recovered moments. $\alpha$ denotes the standard OLS estimator for the intercept. $\beta$ is the standard OLS estimator for the slope in a linear model. $P: \alpha = 0$ denotes the p-value for the two-sided hypothesis test with $H_0: \alpha = 0$. The other p-value columns can be interpreted the same way.}
	\label{Tab: Pred 1}
\end{table}

A very accurate estimator for market moments would result in a linear line with a slope of $1$ and a constant of $0$ which would indicate that all moments have been predicted correctly. In this optimal case, the fitted model would have a very high $R^2$ since it would capture the just described linear relationship. Thus we can interpret the $R^2$ of our regression not merely as an indicator to characterize how well our regression fits the data but also how well our recovered moments predict the realized moments. Furthermore we would expect not to reject the null hypotheses $H_0:\alpha=0$ and $H_0:\beta=1$ if our recovered moments were unbiased estimators. Rejecting the null hypothesis of $H_0: \beta = 0$ is important in order for our recovered moments to have any explanatory power. 


The results of the predictive regression for the first moment in table \ref{Tab: Pred 1} emphasizes that the estimators for $\alpha$ are all very close to zero. For 7, 30 and 91 days maturity, we also cannot reject the null hypothesis under common significance levels, which we are able to for all higher maturities. The intercepts however, except for 1095 days maturity, remain pretty close to zero.


\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{All_rec_returns}
	\label{Fig: Pred Regression Returns}
	\caption{Results of the predictive regression for first recovered moments as explained in equation \ref{EQ: Predicitive Regression}. The 95\% confidence bands are computed from a bootstrap approach.}
\end{figure}

Shorter maturities do have negative beta-estimators of which some are significantly different from zero. Longer maturities seem to have larger betas with the exception of maturity 730. However the data, on which the results of this particular regression is based on, does contain a lot of noise as already examined in \ref{Sec: Data Description}. This should be taken into consideration when examining the results of maturity 730.  

Another interesting observation is, that the $R^2$ of the regression tend to be larger with longer maturities which would be an indicator that longer maturities can predict future realizations better than shorter maturities. A possible explanation for this observation is that the market in the long run is less affected by random short term deviations and noise since these cancel out in the long run. 


\begin{table}[h]
	\centering
	\begin{tabular}{l|ll|ll|l}
		{maturity} &    $\alpha$ & P: $\alpha$ = 0 &     $\beta$ & P: $\beta$ = 0 &      $R^2$ \\
		\midrule
		7    & 0.00109 &     0.00000 &  0.02888 &    0.00000 & 0.04447 \\
		30   & 0.00083 &     0.00003 &  0.36053 &    0.00000 & 0.37034 \\
		91   & 0.00669 &     0.00000 &  0.23481 &    0.00000 & 0.17564 \\
		182  & 0.01068 &     0.00000 &  0.29918 &    0.00000 & 0.16426 \\
		547  & 0.07536 &     0.00000 &  0.00076 &    0.86828 & 0.00001 \\
		730  & 0.08612 &     0.00000 &  0.05255 &    0.00000 & 0.03947 \\
		1095 & 0.15102 &     0.00000 & -0.01915 &    0.18009 & 0.00125 \\
		
	\end{tabular}
	\caption{Predictive Regression for second recovered moments. The columns can be interpreted as described in table: \ref{Tab: Pred 1}}
	\label{Tab: Pred 2}
\end{table}

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{All_rec_variances}
	\label{Fig: Pred Regression Variance}
	\caption{Results of the predictive regression for second recovered moments as explained in equation \ref{EQ: Predicitive Regression}. The 95\% confidence bands are computed from a bootstrap approach.}
\end{figure}

We also computed the predictive regression for the second, third as well as fourth moment and may deduce similar findings regarding the results. 
Within table \ref{Tab: Pred 2} the results show that although alphas are quite close to zero as well, the values are slightly higher. We also can reject the null hypothesis throughout all maturities. Beta-estimators are between zero and one except for maturity 1095, all positive. Hereby the null hypothesis can be rejected for all maturities except 547 as well as 1095. The $R^2$ measure indicates a better prediction power compared to the results of the first moment with the exception of maturity 547. Especially maturity 30,91 and 182 do have quite high $R^2$ values.

\begin{table}[h]
	\centering
	\begin{tabular}{l|ll|ll|l}
		{maturity} &    $\alpha$ & P: $\alpha$ = 0 &     $\beta$ & P: $\beta$ = 0 &      r2 \\
		\midrule
		7    &  0.01511 &     0.77202 & -0.46734 &    0.77060 & 0.00004 \\
		30   & -0.20455 &     0.00259 &  1.87240 &    0.30154 & 0.00070 \\
		91   & -0.59928 &     0.00000 &  3.24057 &    0.00000 & 0.02864 \\
		182  & -0.99458 &     0.00000 &  3.69382 &    0.00000 & 0.08514 \\
		547  & -0.82283 &     0.00000 &  0.16466 &    0.00000 & 0.03459 \\
		730  & -0.82588 &     0.00000 &  0.01173 &    0.66803 & 0.00011 \\
		1095 & -1.55139 &     0.00000 &  0.90549 &    0.00000 & 0.16857 \\
	\end{tabular}
	\caption{Predictive Regression for third recovered moments. The columns can be interpreted as described in table: \ref{Tab: Pred 1}}
	\label{Tab: Pred 3}
\end{table}
\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{All_rec_skewness}
	\label{Fig: Pred Regression Skewness}
	\caption{Results of the predictive regression for third recovered moments as explained in equation \ref{EQ: Predicitive Regression}. The 95\% confidence bands are computed from a bootstrap approach.}
\end{figure}

Table \ref{Tab: Pred 3} indicates that the intercept is negative throughout all maturities but 7 and the negativity mostly increases with maturity, whereas the null hypothesis can be rejected for all maturities except again 7. The slope adopts higher values in comparison with lower moments especially for maturities 30, 91 and 182 and are all positive except maturity 7. The null hypothesis hereby can only be not rejected for maturities 7, 30 and 730. $R^2$ does not show any pattern throughout the maturities and remains quite low except for 182 and 1095 days.

\begin{table}[h]
	\centering
	\begin{tabular}{l|ll|ll|l}
		{maturity} &    $\alpha$ & P: $\alpha$ = 0 &     $\beta$ & P: $\beta$ = 0 &      r2 \\
		\midrule
		7    &  0.03209 &     0.00000 &  0.25006 &    0.00000 & 0.01714 \\
		30   &  0.01910 &     0.20976 &  3.69127 &    0.00000 & 0.15103 \\
		91   &  0.43832 &     0.00000 &  2.04141 &    0.00000 & 0.03190 \\
		182  &  1.23090 &     0.00000 &  1.90597 &    0.00000 & 0.00915 \\
		547  &  6.64742 &     0.00000 & -0.14364 &    0.02580 & 0.00221 \\
		730  &  8.16981 &     0.00000 &  1.07491 &    0.00000 & 0.02859 \\
		1095 & 24.04794 &     0.00000 & -5.38493 &    0.00000 & 0.07790 \\	
	\end{tabular}
	\caption{Predictive Regression for fourth recovered moments. The columns can be interpreted as described in table: \ref{Tab: Pred 1}}
	\label{Tab: Pred 4}
\end{table}

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{All_rec_kurtosis}
	\label{Fig: Pred Regression Lurtosis}
	\caption{Results of the predictive regression for fourth recovered moments as explained in equation \ref{EQ: Predicitive Regression}. The 95\% confidence bands are computed from a bootstrap approach.}
\end{figure}


Within table \ref{Tab: Pred 4} stands out the exponential growth of the $\alpha$ estimator throughout all maturities as the range stretches from close to zero to 24. The null hypothesis can be rejected for all maturities except 30. The estimators for beta increase and decrease throughout the maturities whereas the null hypothesis can be rejected for almost all maturities except 547. $R^2$ remains quite close to zero for all maturities. Extremely striking comes across the last maturity regarding both coefficients with an extremely high value of $\alpha$ as well as a very low negative value of $\beta$.

\section{Support Space}
\label{Sec: Support Space}

As previously mentioned, the approach of \cite{Schneider2017} is based on a series of optimization problems. Each solution consists of the probability vector $d_J$ as well as the corresponding support space $x_J$. We calculate the recovered moments from these values. Since the recovered moments are constructed from the support space  by the approximation of the pricing kernels on the support space, the question arises, whether the support space itself provides useful information about market moments. 

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{SS_and_returns}
	\caption{Insights into the support range (from $x_0$ to  $x_3$) of the recovered time series for maturity 91 days. The ordinate for the support range as well as the strike range is shifted by -1 in oder to obtain a direct comparability with the returns. Returns in this graph are 91 days look ahead returns.}
	\label{Fig: Support Space and Return}
\end{figure}

Figure \ref{Fig: Support Space and Return} provides some insight into the dynamic of the support range, the available strike range as well as the returns. The most conspicuous aspect is that the returns stay within the support range at any given time with only very few exceptions. This finding is very similar to those of \cite{Schneider2017} who also found the support range to be "informative about the range of future possible values for market returns".
A second common finding is that the lower support seems more effected by the available minimum strikes than the higher support.

\section{Pricing Kernels}
\label{Sec: Pricing Kernels}

By using Equation \ref{Eq: Kernels in Vandermonde}, we are able to calculate pricing kernels for each day within our time series. As previously stated in section \ref{ch:whole_approach}, we would expect our kernel projections to be a third degree polynomial. 

\cite{Schneider2017} report to find either flat or U-shaped pricing kernels on the support range. We do find these types of kernels as well, however we also find various other shaped kernels such as locally concave or locally upward sloping forms. 

Figure \ref{Fig: Kernels 2008} provides an overview over some of the recovered pricing kernels from March 2008. Most of these kernels are consistent with the author's findings in the sense that they are either flat downward sloping or U-shaped. All the pricing kernels are only plotted over their support range.

Comparing the pricing kernel from 2007, 2008 and 2009 in figures \ref{Fig: Kernels 2007}, \ref{Fig: Kernels 2008} and \ref{Fig: Kernels 2009}, we can see that the development of a financial crisis does not have a significant effect on the shape of the pricing kernels in our example. It is however worth mentioning that the noise in the support range seems to decrease with increasing maturity. Another interesting finding is that the pricing kernels seem to be more aligned with each other with growing maturity. Whereas short maturities seem to have large differences among pricing kernels, the pricing kernels based on the recovery of longer maturity derivatives seem aligned and resemble a rather flat downward sloping kernel.
This effect seems even stronger in rather stable times compared to those of financial distress.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{Kernels/Plot_2007}
	\caption{Projections of pricing kernels of order $J=3$ from 1\textsuperscript{st} of March 2007 until 10\textsuperscript{th} of March 2007. All pricing kernels are plotted over their support space recovered from the minimal variance projection.}
	\label{Fig: Kernels 2007}
\end{figure}


\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{Kernels/Plot_2008}
	\caption{Projections of pricing kernels of order $J=3$ from 1\textsuperscript{st} of March 2008 until 10\textsuperscript{th} of March 2008. All pricing kernels are plotted over their support space recovered from the minimal variance projection.}
	\label{Fig: Kernels 2008}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{Kernels/Plot_2009}
	\caption{Projections of pricing kernels of order $J=3$ from 1\textsuperscript{st} of March 2009 until 10\textsuperscript{th} of March 2009 in the middle of the crisis. All pricing kernels are plotted over their support space recovered from the minimal variance projection.}
	\label{Fig: Kernels 2009}
\end{figure}
